################################################################################
#
# amlpkg
#
################################################################################

ROOTFS_AMLPKG_DEPENDENCIES = ramdisk rootfs-squashfs host-python

AMLPKG_SRC = $(BR2_EXTERNAL_EmbER_PATH)/release/amlpkg/src
AMLPKG_OUT = $(BINARIES_DIR)/amlpkg

AMLPKG_ARGS = -x $(BR2_AMLPKG_BOARDNAMES)
ROOTFS_AMLPKG_CMD = mkdir -p $(AMLPKG_OUT);

ifeq ($(BR2_AMLPKG_WIPE_DATA),y)
	AMLPKG_ARGS += -w
endif

ifneq ($(strip $(BR2_AMLPKG_BOOTLOADER_IMG)),"")
	AMLPKG_ARGS += -b

	ROOTFS_AMLPKG_CMD += \
		echo "Copy bootloader.img..."; \
		cp -f $(BR2_AMLPKG_BOOTLOADER_IMG) $(AMLPKG_OUT)/bootloader.img 2>/dev/null;

	ADDITIONAL_FILES = bootloader.img
endif

ifneq ($(strip $(BR2_AMLPKG_RECOVERY_IMG)),"")
	AMLPKG_ARGS += -r

	ROOTFS_AMLPKG_CMD += \
		echo "Copy recovery.img..."; \
		cp -f $(BR2_AMLPKG_RECOVERY_IMG) $(AMLPKG_OUT)/recovery.img 2>/dev/null;

	ADDITIONAL_FILES += recovery.img
endif

ifeq ($(BR2_AMLPKG_LOGO_IMG),y)
	AMLPKG_ARGS += -l

	IMG_PACK = $(AMLPKG_SRC)/imgpack
	RES_PACK = $(AMLPKG_SRC)/logos/ember

	IMGPACK = $(IMG_PACK)
	RESPACK = $(RES_PACK)
	ifeq ($(BR2_AMLPKG_IMGPACK_V3),y)
		IMGPACK = $(IMG_PACK)_v3
		RESPACK = $(RES_PACK)_v3
	endif

	ROOTFS_AMLPKG_CMD += \
		echo "Copy logo.img..."; \
		$(IMGPACK) -r $(RESPACK) $(AMLPKG_OUT)/logo.img;

	ADDITIONAL_FILES += logo.img
endif

AMLPKG_ARGS += -s $(BR2_AMLPKG_SYSTEM_PATH)
AMLPKG_ARGS += -d $(BR2_AMLPKG_DATA_PATH)

AMLPKG_ARGS += -n none
ifneq ($(strip $(BR2_AMLPKG_NFTL_PATH)),"")
	AMLPKG_ARGS += -n $(BR2_AMLPKG_NFTL_PATH)
endif

BINARY = update-binary_3.10.x
ifeq ($(BR2_AMLPKG_314_BIN),y)
	BINARY = update-binary_3.14.x
endif

ROOTFS_AMLPKG_CMD += \
	mkdir -p $(AMLPKG_OUT)/META-INF/com/google/android/ 2>/dev/null; \
	PYTHONDONTWRITEBYTECODE=1 $(HOST_DIR)/usr/bin/python $(AMLPKG_SRC)/android_scriptgen $(AMLPKG_ARGS) -i -p $(TARGET_DIR) -o \
	$(AMLPKG_OUT)/META-INF/com/google/android/updater-script; \
	cp -f $(AMLPKG_SRC)/$(BINARY) $(AMLPKG_OUT)/META-INF/com/google/android/update-binary 2>/dev/null;

ifeq ($(BR2_LINUX_KERNEL_DTS_SUPPORT)$(BR2_ARM_AMLOGIC),yy)
	DTS_NAMES = $(call qstrip,$(BR2_LINUX_KERNEL_INTREE_DTS_NAME))
	ifeq ($(BR2_LINUX_KERNEL_USE_CUSTOM_DTS),y)
		DTS_NAMES = $(basename $(notdir $(call qstrip,$(BR2_LINUX_KERNEL_CUSTOM_DTS_PATH))))
	endif

	DTB_NUMBER = $(call words,$(DTS_NAMES))
	DTB_NAMES = $(addsuffix .dtb,$(DTS_NAMES))
	DTB_FILES = $(addprefix $(BINARIES_DIR)/,$(DTB_NAMES))
	DTB_IMG = $(AMLPKG_OUT)/dtb.img

	BUILD_DTB_IMG = cp -f $(DTB_FILES) $(DTB_IMG) 2>/dev/null;
	ifneq ($(DTB_NUMBER),1)
		BUILD_DTB_IMG = \
			cp -f $(DTB_FILES) $(AMLPKG_OUT)/ 2>/dev/null; \
			$(AMLPKG_SRC)/dtbTool -o $(DTB_IMG) -p $(LINUX_DIR)/scripts/dtc/ $(AMLPKG_OUT)/ 2>/dev/null;
	endif

	# Amlogic GX - Install DTS to flash.
	ifeq ($(BR2_BOARD_TYPE_AMLOGIC_GX),y)
		AMLPKG_ARGS += -t
		ADDITIONAL_FILES += dtb.img
	endif

	APPEND_DTS = --second $(DTB_IMG)
endif

IMAGE = uImage
ifeq ($(BR2_LINUX_KERNEL_IMAGE),y)
	IMAGE = Image
endif

UPDATE_ZIP = EmbER-$(BR2_VERSION)-$(shell sed 's/ /_/g' <<< $(BR2_AMLPKG_BOARDNAMES))-$(shell date -u +%Y%m%d).zip
ifneq ($(strip $(BR2_AMLPKG_CUSTOM_ZIP_NAME)),"")
UPDATE_ZIP = $(call qstrip,$(BR2_AMLPKG_CUSTOM_ZIP_NAME))
endif

ROOTFS_AMLPKG_CMD += \
	echo "Creating boot.img..."; \
	$(BUILD_DTB_IMG) \
	$(AMLPKG_SRC)/mkbootimg --kernel $(BINARIES_DIR)/$(IMAGE) --ramdisk $(BINARIES_DIR)/ramdisk.gz $(APPEND_DTS) -o $(AMLPKG_OUT)/boot.img 2>/dev/null; \
	cd $(AMLPKG_OUT); \
	cp -f $(BINARIES_DIR)/rootfs.squashfs .; \
	zip -q -r $(AMLPKG_OUT)/update-unsigned.zip $(ADDITIONAL_FILES) boot.img META-INF rootfs.squashfs; \
	echo "Signing $(UPDATE_ZIP)..."; \
	cd $(AMLPKG_SRC); \
	java -Xmx1024m -jar signapk.jar -w testkey.x509.pem testkey.pk8 $(AMLPKG_OUT)/update-unsigned.zip '$(BINARIES_DIR)/$(UPDATE_ZIP)'; \
	rm -rf $(AMLPKG_OUT)

$(eval $(rootfs))
