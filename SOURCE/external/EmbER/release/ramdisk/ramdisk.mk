################################################################################
#
# ramdisk
#
################################################################################

RAMDISK_VERSION = 1.0.8
RAMDISK_SITE = $(BR2_EXTERNAL_EmbER_PATH)/release/ramdisk/src
RAMDISK_SITE_METHOD = local
RAMDISK_DEPENDENCIES = busybox e2fsprogs fbset fbv host-fakeroot

RAMDISK_OUT = $(@D)/ramdisk

# Base
RAMDISK_BUILD_CMDS = \
	for dir in .ovfs bin boot dev etc lib media proc rootfs sys system tmp; do \
		mkdir -p $(RAMDISK_OUT)/$$dir; \
	done; \
	$(HOST_DIR)/bin/fakeroot -- sh -c "mknod -m 600 $(RAMDISK_OUT)/dev/console c 5 1";

ifeq ($(BR2_ARCH_IS_64),y)
RAMDISK_BUILD_CMDS += ln -sf /lib $(RAMDISK_OUT)/lib64;
endif

# Init
RAMDISK_BUILD_CMDS += $(INSTALL) -m 755 $(@D)/init $(RAMDISK_OUT)/;

# Platform-init
ifeq ($(strip $(BR2_RAMDISK_PLATFORM_INIT)),"")
BR2_RAMDISK_PLATFORM_INIT = $(@D)/platform-init
endif
RAMDISK_BUILD_CMDS += $(INSTALL) -m 755 $(call qstrip,$(BR2_RAMDISK_PLATFORM_INIT)) $(RAMDISK_OUT)/bin/platform-init;

# Data
ifneq ($(strip $(BR2_RAMDISK_DATA_PART)),"")
RAMDISK_BUILD_CMDS += sed -i -e "s|DATA=\"\"|DATA=\"$(call qstrip,$(BR2_RAMDISK_DATA_PART))\"|" $(RAMDISK_OUT)/init;
else
$(error BR2_RAMDISK_DATA_PART is not set)
endif

# System
ifneq ($(strip $(BR2_RAMDISK_SYSTEM_PART)),"")
RAMDISK_BUILD_CMDS += sed -i -e "s|SYSTEM=\"\"|SYSTEM=\"$(call qstrip,$(BR2_RAMDISK_SYSTEM_PART))\"|" $(RAMDISK_OUT)/init;
else
$(error BR2_RAMDISK_SYSTEM_PART is not set)
endif

# Boot
ifneq ($(strip $(BR2_RAMDISK_MODULES)),"")
MODULES = $(foreach module,$(call qstrip,$(BR2_RAMDISK_MODULES)),$(shell find $(TARGET_DIR)/lib/modules/ -name $(module)))
RAMDISK_BUILD_CMDS += \
	$(INSTALL) -m 644 -t $(RAMDISK_OUT)/boot $(MODULES); \
	sed -i -e "s|MODULES=\"\"|MODULES=\"$(call qstrip,$(BR2_RAMDISK_MODULES))\"|" $(RAMDISK_OUT)/init;
else
RAMDISK_BUILD_CMDS += sed -i -e "/MODULES=\"\"/d" $(RAMDISK_OUT)/init;
endif

# Bin
RAMDISK_BUILD_CMDS += \
	$(INSTALL) -m 755 $(TARGET_DIR)/bin/busybox $(RAMDISK_OUT)/bin/; \
	for applet in awk blkid cat chmod cp cut echo grep insmod ln ls mkdir mount rm sh switch_root sync touch true umount xargs; do \
		ln -sf busybox $(RAMDISK_OUT)/bin/$$applet; \
	done;

# Lib
RAMDISK_BUILD_CMDS += \
	cp -a $(TARGET_DIR)/lib/ld-*.so* $(RAMDISK_OUT)/lib/; \
	cp -a $(TARGET_DIR)/lib/libc-*.so* $(RAMDISK_OUT)/lib/; \
	cp -a $(TARGET_DIR)/lib/libc.so* $(RAMDISK_OUT)/lib/; \
	cp -a $(TARGET_DIR)/lib/libdl-*.so* $(RAMDISK_OUT)/lib/; \
	cp -a $(TARGET_DIR)/lib/libdl.so* $(RAMDISK_OUT)/lib/; \
	cp -a $(TARGET_DIR)/lib/libm-*.so* $(RAMDISK_OUT)/lib/; \
	cp -a $(TARGET_DIR)/lib/libm.so* $(RAMDISK_OUT)/lib/; \
	cp -a $(TARGET_DIR)/lib/libnsl-*.so* $(RAMDISK_OUT)/lib/; \
	cp -a $(TARGET_DIR)/lib/libnsl.so* $(RAMDISK_OUT)/lib/; \
	cp -a $(TARGET_DIR)/lib/libpthread-*.so* $(RAMDISK_OUT)/lib/; \
	cp -a $(TARGET_DIR)/lib/libpthread.so* $(RAMDISK_OUT)/lib/;

ifeq ($(BR2_PACKAGE_LIBTIRPC),y)
RAMDISK_DEPENDENCIES += libtirpc
RAMDISK_BUILD_CMDS += cp -a $(TARGET_DIR)/usr/lib/libtirpc.so* $(RAMDISK_OUT)/lib/;
endif

# Splash
ifeq ($(strip $(BR2_RAMDISK_SPLASH_DIR)),"")
BR2_RAMDISK_SPLASH_DIR = $(@D)
endif
RAMDISK_BUILD_CMDS += \
	$(INSTALL) -m 755 $(@D)/splash $(RAMDISK_OUT)/bin/; \
	cp -a $(call qstrip,$(BR2_RAMDISK_SPLASH_DIR))/SplashHD.jpg $(RAMDISK_OUT)/media/; \
	cp -a $(call qstrip,$(BR2_RAMDISK_SPLASH_DIR))/SplashSD.jpg $(RAMDISK_OUT)/media/;

# Fbset
RAMDISK_BUILD_CMDS += $(INSTALL) -m 755 $(TARGET_DIR)/usr/sbin/fbset $(RAMDISK_OUT)/bin/;

# Fbv
RAMDISK_BUILD_CMDS += \
	$(INSTALL) -m 755 $(TARGET_DIR)/usr/bin/fbv $(RAMDISK_OUT)/bin/; \
	cp -a $(TARGET_DIR)/usr/lib/libjpeg.so* $(RAMDISK_OUT)/lib/;

ifeq ($(BR2_PACKAGE_FBV_GIF),y)
RAMDISK_BUILD_CMDS += cp -a $(TARGET_DIR)/usr/lib/libgif.so* $(RAMDISK_OUT)/lib/;
endif

ifeq ($(BR2_PACKAGE_FBV_PNG),y)
RAMDISK_BUILD_CMDS += \
	cp -a $(TARGET_DIR)/lib/libz.so* $(RAMDISK_OUT)/lib/; \
	cp -a $(TARGET_DIR)/usr/lib/libpng*.so* $(RAMDISK_OUT)/lib/;
endif

# E2fsck / Mke2fs
RAMDISK_BUILD_CMDS += \
	$(INSTALL) -m 755 $(TARGET_DIR)/usr/sbin/e2fsck $(RAMDISK_OUT)/bin/; \
	$(INSTALL) -m 755 $(TARGET_DIR)/usr/sbin/mke2fs $(RAMDISK_OUT)/bin/; \
	cp -a $(TARGET_DIR)/lib/libblkid.so* $(RAMDISK_OUT)/lib/; \
	cp -a $(TARGET_DIR)/usr/lib/libcom_err.so* $(RAMDISK_OUT)/lib/; \
	cp -a $(TARGET_DIR)/usr/lib/libe2p.so* $(RAMDISK_OUT)/lib/; \
	cp -a $(TARGET_DIR)/usr/lib/libext2fs.so* $(RAMDISK_OUT)/lib/; \
	cp -a $(TARGET_DIR)/lib/libuuid.so* $(RAMDISK_OUT)/lib/; \
	ln -sf /proc/mounts $(RAMDISK_OUT)/etc/mtab;

# Fix broken symlinks
RAMDISK_BUILD_CMDS += \
	for symlink in $$(find $(RAMDISK_OUT) -type l -xtype l | xargs); do \
		ln -sf $$(readlink $$symlink | xargs basename) $$symlink; \
	done

# Package ramdisk
RAMDISK_INSTALL_TARGET_CMDS += \
	cd $(RAMDISK_OUT); \
	find . | cpio -o --format=newc | gzip > $(BINARIES_DIR)/ramdisk.gz 2>/dev/null; \
	cd $(TOPDIR); \
	sed -i -e "/\/dev\/root/d" $(TARGET_DIR)/etc/fstab

$(eval $(generic-package))
