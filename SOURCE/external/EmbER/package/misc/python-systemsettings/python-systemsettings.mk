################################################################################
#
# python-systemsettings
#
################################################################################

PYTHON_SYSTEMSETTINGS_VERSION = d3da4a9
PYTHON_SYSTEMSETTINGS_SITE = git@gitlab.com:ember-dev/python-systemsettings.git
PYTHON_SYSTEMSETTINGS_SITE_METHOD = git
PYTHON_SYSTEMSETTINGS_INSTALL_STAGING = NO
PYTHON_SYSTEMSETTINGS_SETUP_TYPE = setuptools

$(eval $(python-package))
