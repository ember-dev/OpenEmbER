################################################################################
#
# smb_conf
#
################################################################################

SMB_CONF_SITE = $(BR2_EXTERNAL_EmbER_PATH)/package/misc/smb_conf/src
SMB_CONF_VERSION = 1.0.3
SMB_CONF_SITE_METHOD = local

define SMB_CONF_INSTALL_TARGET_CMDS
	$(INSTALL) -m 644 -D $(@D)/smb.conf.default $(TARGET_DIR)/etc/samba/smb.conf.default
endef

define SMB_CONF_INSTALL_INIT_SYSV
	$(INSTALL) -m 755 -D $(@D)/smb-conf $(TARGET_DIR)/etc/init.d/S90smb-conf
endef

define SMB_CONF_INSTALL_INIT_SYSTEMD
	$(INSTALL) -m 755 -D $(@D)/smb-conf $(TARGET_DIR)/usr/bin/ember/smb-conf
	$(INSTALL) -m 644 -D $(@D)/smb-conf.service $(TARGET_DIR)/usr/lib/systemd/system/smb-conf.service
	mkdir -p $(TARGET_DIR)/etc/systemd/system/multi-user.target.wants
	ln -fs ../../../../usr/lib/systemd/system/smb-conf.service $(TARGET_DIR)/etc/systemd/system/multi-user.target.wants/smb-conf.service
endef

$(eval $(generic-package))
