################################################################################
#
# kodi-jsonschemabuilder
#
################################################################################

KODI_JSONSCHEMABUILDER_VERSION = $(call qstrip,$(BR2_PACKAGE_KODI_REV))
KODI_JSONSCHEMABUILDER_SITE = $(call qstrip,$(BR2_PACKAGE_KODI_GIT))
KODI_JSONSCHEMABUILDER_SITE_METHOD = git
KODI_JSONSCHEMABUILDER_SOURCE = kodi-$(KODI_JSONSCHEMABUILDER_VERSION).tar.gz
KODI_JSONSCHEMABUILDER_LICENSE = GPL-2.0
KODI_JSONSCHEMABUILDER_LICENSE_FILES = LICENSE.GPL
HOST_KODI_JSONSCHEMABUILDER_SUBDIR = tools/depends/native/JsonSchemaBuilder

HOST_KODI_JSONSCHEMABUILDER_CONF_OPTS = \
	-DCMAKE_MODULE_PATH=$(@D)/project/cmake/modules

define HOST_KODI_JSONSCHEMABUILDER_INSTALL_CMDS
	$(INSTALL) -m 755 -D \
		$(@D)/tools/depends/native/JsonSchemaBuilder/JsonSchemaBuilder \
		$(HOST_DIR)/bin/JsonSchemaBuilder
endef

$(eval $(host-cmake-package))
