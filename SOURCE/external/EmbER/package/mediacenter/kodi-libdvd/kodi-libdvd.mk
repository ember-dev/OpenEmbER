################################################################################
#
# kodi-libdvd
#
################################################################################

KODI_LIBDVDCSS_VERSION = 2f12236
KODI_LIBDVDNAV_VERSION = 981488f
KODI_LIBDVDREAD_VERSION = 17d99db

KODI_LIBDVD_EXTRA_DOWNLOADS = \
	https://github.com/xbmc/libdvdcss/archive/$(KODI_LIBDVDCSS_VERSION).tar.gz \
	https://github.com/xbmc/libdvdnav/archive/$(KODI_LIBDVDNAV_VERSION).tar.gz \
	https://github.com/xbmc/libdvdread/archive/$(KODI_LIBDVDREAD_VERSION).tar.gz

$(eval $(virtual-package))
