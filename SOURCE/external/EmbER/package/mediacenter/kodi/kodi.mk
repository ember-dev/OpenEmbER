################################################################################
#
# kodi
#
################################################################################

KODI_VERSION = $(call qstrip,$(BR2_PACKAGE_KODI_REV))
KODI_SITE = $(call qstrip,$(BR2_PACKAGE_KODI_GIT))
KODI_SITE_METHOD = git
KODI_LICENSE = GPL-2.0
KODI_LICENSE_FILES = LICENSE.GPL

# needed for binary addons
KODI_INSTALL_STAGING = YES

KODI_DEPENDENCIES = \
	bzip2 \
	expat \
	ffmpeg \
	fontconfig \
	freetype \
	host-gawk \
	host-gperf \
	host-kodi-jsonschemabuilder \
	host-kodi-texturepacker \
	host-nasm \
	host-swig \
	host-zip \
	kodi-libdvd \
	libass \
	libcdio \
	libcrossguid \
	libcurl \
	libfribidi \
	libplist \
	libsamplerate \
	lzo \
	ncurses \
	openssl \
	pcre \
	python \
	readline \
	sqlite \
	taglib \
	tinyxml \
	yajl \
	zlib

KODI_SUBDIR = project/cmake

KODI_CONF_OPTS += \
	-DCMAKE_C_FLAGS="$(TARGET_CFLAGS) $(KODI_C_FLAGS)" \
	-DCMAKE_CXX_FLAGS="$(TARGET_CXXFLAGS) $(KODI_CXX_FLAGS)" \
	-DENABLE_DVDCSS=ON \
	-DENABLE_INTERNAL_CROSSGUID=OFF \
	-DENABLE_INTERNAL_FFMPEG=OFF \
	-DKODI_DEPENDSBUILD=OFF \
	-DENABLE_OPENSSL=ON \
	-DNATIVEPREFIX=$(HOST_DIR) \
	-DDEPENDS_PATH=$(@D) \
	-DWITH_FFMPEG=$(STAGING_DIR)/usr \
	-DWITH_TEXTUREPACKER=$(HOST_DIR)/bin/TexturePacker \
	-DLIBDVDCSS_URL=$(DL_DIR)/$(KODI_LIBDVDCSS_VERSION).tar.gz \
	-DLIBDVDNAV_URL=$(DL_DIR)/$(KODI_LIBDVDNAV_VERSION).tar.gz \
	-DLIBDVDREAD_URL=$(DL_DIR)/$(KODI_LIBDVDREAD_VERSION).tar.gz \
	-DGIT_VERSION=$(shell expr substr $(KODI_VERSION) 1 7)

ifeq ($(BR2_ENABLE_LOCALE),)
KODI_DEPENDENCIES += libiconv
endif

ifeq ($(BR2_PACKAGE_RPI_USERLAND),y)
KODI_CONF_OPTS += -DCORE_SYSTEM_NAME=rbpi
KODI_DEPENDENCIES += rpi-userland
# These CPU-specific options are only used on rbpi:
# https://github.com/xbmc/xbmc/blob/Krypton/project/cmake/scripts/rbpi/ArchSetup.cmake#L13
ifeq ($(BR2_arm1176jzf_s)$(BR2_cortex_a7)$(BR2_cortex_a53),y)
KODI_CONF_OPTS += -DWITH_CPU=$(BR2_GCC_TARGET_CPU)
endif
else
ifeq ($(BR2_arceb)$(BR2_arcle),y)
KODI_CONF_OPTS += -DWITH_ARCH=arc -DWITH_CPU=arc
else ifeq ($(BR2_armeb),y)
KODI_CONF_OPTS += -DWITH_ARCH=arm -DWITH_CPU=arm
else ifeq ($(BR2_mips)$(BR2_mipsel)$(BR2_mips64)$(BR2_mips64el),y)
KODI_CONF_OPTS += \
	-DWITH_ARCH=mips$(if $(BR2_ARCH_IS_64),64) \
	-DWITH_CPU=mips$(if $(BR2_ARCH_IS_64),64)
else ifeq ($(BR2_powerpc)$(BR2_powerpc64le),y)
KODI_CONF_OPTS += \
	-DWITH_ARCH=powerpc$(if $(BR2_ARCH_IS_64),64) \
	-DWITH_CPU=powerpc$(if $(BR2_ARCH_IS_64),64)
else ifeq ($(BR2_powerpc64)$(BR2_sparc64)$(BR2_sh4)$(BR2_xtensa),y)
KODI_CONF_OPTS += -DWITH_ARCH=$(BR2_ARCH) -DWITH_CPU=$(BR2_ARCH)
else
# Kodi auto-detects ARCH, tested: arm, aarch64, i386, x86_64
# see project/cmake/scripts/linux/ArchSetup.cmake
KODI_CONF_OPTS += -DWITH_CPU=$(BR2_ARCH)
endif
endif

ifeq ($(BR2_ARM_CPU_HAS_NEON),y)
KODI_CONF_OPTS += -DNEON=ON -DNEON_FLAGS="-mfpu=neon -mvectorize-with-neon-quad"
else
KODI_CONF_OPTS += -DNEON=OFF
endif

ifeq ($(BR2_X86_CPU_HAS_SSE),y)
KODI_CONF_OPTS += -D_SSE_OK=ON -D_SSE_TRUE=ON
else
KODI_CONF_OPTS += -D_SSE_OK=OFF -D_SSE_TRUE=OFF
endif

ifeq ($(BR2_X86_CPU_HAS_SSE2),y)
KODI_CONF_OPTS += -D_SSE2_OK=ON -D_SSE2_TRUE=ON
else
KODI_CONF_OPTS += -D_SSE2_OK=OFF -D_SSE2_TRUE=OFF
endif

ifeq ($(BR2_X86_CPU_HAS_SSE3),y)
KODI_CONF_OPTS += -D_SSE3_OK=ON -D_SSE3_TRUE=ON
else
KODI_CONF_OPTS += -D_SSE3_OK=OFF -D_SSE3_TRUE=OFF
endif

ifeq ($(BR2_X86_CPU_HAS_SSSE3),y)
KODI_CONF_OPTS += -D_SSSE3_OK=ON -D_SSSE3_TRUE=ON
else
KODI_CONF_OPTS += -D_SSSE3_OK=OFF -D_SSSE3_TRUE=OFF
endif

ifeq ($(BR2_X86_CPU_HAS_SSE4),y)
KODI_CONF_OPTS += -D_SSE41_OK=ON -D_SSE41_TRUE=ON
else
KODI_CONF_OPTS += -D_SSE41_OK=OFF -D_SSE41_TRUE=OFF
endif

ifeq ($(BR2_X86_CPU_HAS_SSE42),y)
KODI_CONF_OPTS += -D_SSE42_OK=ON -D_SSE42_TRUE=ON
else
KODI_CONF_OPTS += -D_SSE42_OK=OFF -D_SSE42_TRUE=OFF
endif

ifeq ($(BR2_X86_CPU_HAS_AVX),y)
KODI_CONF_OPTS += -D_AVX_OK=ON -D_AVX_TRUE=ON
else
KODI_CONF_OPTS += -D_AVX_OK=OFF -D_AVX_TRUE=OFF
endif

ifeq ($(BR2_X86_CPU_HAS_AVX2),y)
KODI_CONF_OPTS += -D_AVX2_OK=ON -D_AVX2_TRUE=ON
else
KODI_CONF_OPTS += -D_AVX2_OK=OFF -D_AVX2_TRUE=OFF
endif

# mips: uses __atomic_load_8
ifeq ($(BR2_TOOLCHAIN_HAS_LIBATOMIC),y)
KODI_CXX_FLAGS += -latomic
endif

ifeq ($(BR2_PACKAGE_KODI_MYSQL),y)
KODI_CONF_OPTS += -DENABLE_MYSQLCLIENT=ON
KODI_DEPENDENCIES += mysql
else
KODI_CONF_OPTS += -DENABLE_MYSQLCLIENT=OFF
endif

ifeq ($(BR2_PACKAGE_KODI_NONFREE),y)
KODI_CONF_OPTS += -DENABLE_NONFREE=ON
KODI_LICENSE := $(KODI_LICENSE), unrar
KODI_LICENSE_FILES += lib/UnrarXLib/license.txt
else
KODI_CONF_OPTS += -DENABLE_NONFREE=OFF
endif

ifeq ($(BR2_PACKAGE_RPI_USERLAND),y)
KODI_CONF_OPTS += -DCORE_SYSTEM_NAME=rbpi
KODI_DEPENDENCIES += rpi-userland
else
# Kodi considers "rpbi" and "linux" as two separate platforms. The
# below options, defined in
# project/cmake/scripts/linux/ArchSetup.cmake are only valid for the
# "linux" platforms. The "rpbi" platform has a different set of
# options, defined in project/cmake/scripts/rbpi/
KODI_CONF_OPTS += -DENABLE_LDGOLD=OFF
ifeq ($(BR2_PACKAGE_LIBAMCODEC),y)
KODI_CONF_OPTS += -DENABLE_AML=ON
KODI_DEPENDENCIES += libamcodec
else
KODI_CONF_OPTS += -DENABLE_AML=OFF
endif
ifeq ($(BR2_PACKAGE_IMX_VPUWRAP),y)
KODI_CONF_OPTS += -DENABLE_IMX=ON
KODI_DEPENDENCIES += imx-vpuwrap
else
KODI_CONF_OPTS += -DENABLE_IMX=OFF
endif
endif

ifeq ($(BR2_PACKAGE_HAS_UDEV),y)
KODI_CONF_OPTS += -DENABLE_UDEV=ON
KODI_DEPENDENCIES += udev
else
KODI_CONF_OPTS += -DENABLE_UDEV=OFF
ifeq ($(BR2_PACKAGE_KODI_LIBUSB),y)
KODI_CONF_OPTS += -DENABLE_LIBUSB=ON
KODI_DEPENDENCIES += libusb-compat
endif
endif

ifeq ($(BR2_PACKAGE_LIBCAP),y)
KODI_CONF_OPTS += -DENABLE_CAP=ON
KODI_DEPENDENCIES += libcap
else
KODI_CONF_OPTS += -DENABLE_CAP=OFF
endif

ifeq ($(BR2_PACKAGE_LIBXML2)$(BR2_PACKAGE_LIBXSLT),yy)
KODI_CONF_OPTS += -DENABLE_XSLT=ON
KODI_DEPENDENCIES += libxml2 libxslt
else
KODI_CONF_OPTS += -DENABLE_XSLT=OFF
endif

ifeq ($(BR2_PACKAGE_KODI_BLUEZ),y)
KODI_CONF_OPTS += -DENABLE_BLUETOOTH=ON
KODI_DEPENDENCIES += bluez5_utils
else
KODI_CONF_OPTS += -DENABLE_BLUETOOTH=OFF
endif

ifeq ($(BR2_PACKAGE_KODI_DBUS),y)
KODI_DEPENDENCIES += dbus
KODI_CONF_OPTS += -DENABLE_DBUS=ON
else
KODI_CONF_OPTS += -DENABLE_DBUS=OFF
endif

ifeq ($(BR2_PACKAGE_KODI_EVENTCLIENTS),y)
KODI_CONF_OPTS += -DENABLE_EVENTCLIENTS=ON
else
KODI_CONF_OPTS += -DENABLE_EVENTCLIENTS=OFF
endif

ifeq ($(BR2_PACKAGE_KODI_ALSA_LIB),y)
KODI_CONF_OPTS += -DENABLE_ALSA=ON
KODI_DEPENDENCIES += alsa-lib
else
KODI_CONF_OPTS += -DENABLE_ALSA=OFF
endif

ifeq ($(BR2_PACKAGE_KODI_GL_EGL),y)
KODI_DEPENDENCIES += libegl libglu libgl xlib_libX11 xlib_libXext \
	xlib_libXrandr libdrm
KODI_CONF_OPTS += -DENABLE_OPENGL=ON -DENABLE_X11=ON -DENABLE_OPENGLES=OFF
else
KODI_CONF_OPTS += -DENABLE_OPENGL=OFF -DENABLE_X11=OFF
endif

ifeq ($(BR2_PACKAGE_KODI_EGL_GLES),y)
KODI_DEPENDENCIES += libegl libgles
KODI_CONF_OPTS += -DENABLE_OPENGLES=ON
KODI_C_FLAGS += `$(PKG_CONFIG_HOST_BINARY) --cflags --libs egl`
KODI_CXX_FLAGS += `$(PKG_CONFIG_HOST_BINARY) --cflags --libs egl`
else
KODI_CONF_OPTS += -DENABLE_OPENGLES=OFF
endif

ifeq ($(BR2_PACKAGE_KODI_LIBMICROHTTPD),y)
KODI_CONF_OPTS += -DENABLE_MICROHTTPD=ON
KODI_DEPENDENCIES += libmicrohttpd
else
KODI_CONF_OPTS += -DENABLE_MICROHTTPD=OFF
endif

ifeq ($(BR2_PACKAGE_KODI_LIBSMBCLIENT),y)
KODI_DEPENDENCIES += samba4
KODI_CONF_OPTS += -DENABLE_SMBCLIENT=ON
else
KODI_CONF_OPTS += -DENABLE_SMBCLIENT=OFF
endif

ifeq ($(BR2_PACKAGE_KODI_LIBNFS),y)
KODI_DEPENDENCIES += libnfs
KODI_CONF_OPTS += -DENABLE_NFS=ON
else
KODI_CONF_OPTS += -DENABLE_NFS=OFF
endif

ifeq ($(BR2_PACKAGE_KODI_LIBBLURAY),y)
KODI_DEPENDENCIES += libbluray
KODI_CONF_OPTS += -DENABLE_BLURAY=ON
else
KODI_CONF_OPTS += -DENABLE_BLURAY=OFF
endif

ifeq ($(BR2_PACKAGE_KODI_LIBSHAIRPLAY),y)
KODI_DEPENDENCIES += libshairplay
KODI_CONF_OPTS += -DENABLE_AIRTUNES=ON
else
KODI_CONF_OPTS += -DENABLE_AIRTUNES=OFF
endif

ifeq ($(BR2_PACKAGE_KODI_LIBSSH),y)
KODI_DEPENDENCIES += libssh
KODI_CONF_OPTS += -DENABLE_SSH=ON
else
KODI_CONF_OPTS += -DENABLE_SSH=OFF
endif

ifeq ($(BR2_PACKAGE_KODI_AVAHI),y)
KODI_DEPENDENCIES += avahi
KODI_CONF_OPTS += -DENABLE_AVAHI=ON
else
KODI_CONF_OPTS += -DENABLE_AVAHI=OFF
endif

ifeq ($(BR2_PACKAGE_KODI_LIBCEC),y)
KODI_DEPENDENCIES += libcec
KODI_CONF_OPTS += -DENABLE_CEC=ON
else
KODI_CONF_OPTS += -DENABLE_CEC=OFF
endif

ifeq ($(BR2_PACKAGE_KODI_LCMS2),y)
KODI_DEPENDENCIES += lcms2
KODI_CONF_OPTS += -DENABLE_LCMS2=ON
else
KODI_CONF_OPTS += -DENABLE_LCMS2=OFF
endif

ifeq ($(BR2_PACKAGE_KODI_LIRC),y)
KODI_CONF_OPTS += -DENABLE_LIRC=ON
else
KODI_CONF_OPTS += -DENABLE_LIRC=OFF
endif

ifeq ($(BR2_PACKAGE_KODI_LIBTHEORA),y)
KODI_DEPENDENCIES += libtheora
endif

# kodi needs libva & libva-glx
ifeq ($(BR2_PACKAGE_KODI_LIBVA)$(BR2_PACKAGE_MESA3D_DRI_DRIVER),yy)
KODI_DEPENDENCIES += mesa3d libva
KODI_CONF_OPTS += -DENABLE_VAAPI=ON
else
KODI_CONF_OPTS += -DENABLE_VAAPI=OFF
endif

ifeq ($(BR2_PACKAGE_KODI_LIBVDPAU),y)
KODI_DEPENDENCIES += libvdpau
KODI_CONF_OPTS += -DENABLE_VDPAU=ON
else
KODI_CONF_OPTS += -DENABLE_VDPAU=OFF
endif

ifeq ($(BR2_PACKAGE_KODI_UPNP),y)
KODI_CONF_OPTS += -DENABLE_UPNP=ON
else
KODI_CONF_OPTS += -DENABLE_UPNP=OFF
endif

ifeq ($(BR2_PACKAGE_KODI_OPTICALDRIVE),y)
KODI_CONF_OPTS += -DENABLE_OPTICAL=ON
else
KODI_CONF_OPTS += -DENABLE_OPTICAL=OFF
endif

ifeq ($(BR2_PACKAGE_KODI_PULSEAUDIO),y)
KODI_CONF_OPTS += -DENABLE_PULSEAUDIO=ON
KODI_DEPENDENCIES += pulseaudio
else
KODI_CONF_OPTS += -DENABLE_PULSEAUDIO=OFF
endif

ifeq ($(BR2_CCACHE),y)
KODI_CONF_OPTS += -DENABLE_CCACHE=ON
else
KODI_CONF_OPTS += -DENABLE_CCACHE=OFF
endif

# Remove unneeded addons
SRC_ADDON_MANIFEST = $(@D)/system/addon-manifest.xml
SRC_CMAKE_LIST = $(@D)/project/cmake/CMakeLists.txt
define KODI_CLEAN_UNUSED_ADDONS
	# estouchy
	rm -rf $(@D)/addons/skin.estouchy
	sed -i '/skin.estouchy/d' $(SRC_ADDON_MANIFEST)
	sed -i '/skin.estouchy/d' $(SRC_CMAKE_LIST)
	# estuary
	rm -rf $(@D)/addons/skin.estuary
	sed -i '/skin.estuary/d' $(SRC_ADDON_MANIFEST)
	sed -i '/skin.estuary/d' $(SRC_CMAKE_LIST)
	# version check
	rm -rf $(@D)/addons/service.xbmc.versioncheck
	sed -i '/service.xbmc.versioncheck/d' $(SRC_ADDON_MANIFEST)
endef
KODI_PRE_BUILD_HOOKS += KODI_CLEAN_UNUSED_ADDONS

# Install init
define KODI_INSTALL_INIT_SYSTEMD
	$(INSTALL) -D -m 0644 $(BR2_EXTERNAL_EmbER_PATH)/package/mediacenter/kodi/kodi.service \
		$(TARGET_DIR)/usr/lib/systemd/system/kodi.service

	mkdir -p $(TARGET_DIR)/etc/systemd/system/multi-user.target.wants

	ln -fs ../../../../usr/lib/systemd/system/kodi.service \
		$(TARGET_DIR)/etc/systemd/system/multi-user.target.wants/kodi.service
endef

# Include extra makefile
include $(BR2_EXTERNAL_EmbER_PATH)/package/mediacenter/kodi/kodi.mk.ext

# Strip binaries
define KODI_STRIP_BINARIES
	find $(TARGET_DIR)/usr/lib/kodi/ -name "*.so" -exec $(STRIPCMD) $(STRIP_STRIP_UNNEEDED) {} \;
	$(STRIPCMD) $(STRIP_STRIP_UNNEEDED) $(TARGET_DIR)/usr/lib/kodi/kodi.bin
endef
ifneq ($(BR2_ENABLE_DEBUG),y)
	KODI_POST_INSTALL_TARGET_HOOKS += KODI_STRIP_BINARIES
endif

$(eval $(cmake-package))
