################################################################################
#
# Embedded Entertainment Rom (EmbER)
#
################################################################################

export HOME="/root"

for config in $HOME/.kodi/addons/*/profile.d/*.profile; do
	if [ -f "$config" ]; then
		. $config
	fi
done

oe_setup_addon()
{
	if [ ! -z "$1" ]; then
		DEF="$HOME/.kodi/addons/$1/settings-default.xml"
		CUR="$HOME/.kodi/userdata/addon_data/$1/settings.xml"

		# export some useful variables
		ADDON_DIR="$HOME/.kodi/addons/$1"
		ADDON_HOME="$HOME/.kodi/userdata/addon_data/$1"
		ADDON_LOG_FILE="$ADDON_HOME/service.log"

		[ ! -d "$ADDON_HOME" ] && mkdir -p $ADDON_HOME

		# copy defaults
		if [ -f "$DEF" -a ! -f "$CUR" ]; then
			cp $DEF $CUR
		fi

		# parse config
		[ -f "$DEF" ] && eval $(cat "$DEF" | awk -F\" '{print $2"=\""$4"\""}' | sed '/^=/d')
		[ -f "$CUR" ] && eval $(cat "$CUR" | awk -F\" '{print $2"=\""$4"\""}' | sed '/^=/d')
	fi
}

for addon in $HOME/.kodi/addons/*/bin /usr/lib/kodi/addons/*/bin; do
	[ -d "$addon" ] && PATH="$PATH:$addon"
done
export PATH

LD_LIBRARY_PATH="/usr/lib:/usr/lib/pulseaudio"
for addon in $HOME/.kodi/addons/*/lib /usr/lib/kodi/addons/*/lib; do
	[ -d "$addon" ] && LD_LIBRARY_PATH="$LD_LIBRARY_PATH:$addon"
done
export LD_LIBRARY_PATH
