################################################################################
#
# kodi-skin-estuary
#
################################################################################

KODI_SKIN_ESTUARY_VERSION = $(call qstrip,$(BR2_PACKAGE_KODI_REV))
KODI_SKIN_ESTUARY_SITE = $(call github,xbmc,xbmc,$(KODI_SKIN_ESTUARY_VERSION))
KODI_SKIN_ESTUARY_SOURCE = kodi-$(KODI_SKIN_ESTUARY_VERSION).tar.gz
KODI_SKIN_ESTUARY_LICENSE = Creative Commons Attribution-ShareAlike 4.0 Unported License, GPL-2.0
KODI_SKIN_ESTUARY_LICENSE_FILES = LICENSE.txt
KODI_SKIN_ESTUARY_DEPENDENCIES = kodi

SRC_DIR = $(@D)/addons/skin.estuary

define KODI_SKIN_ESTUARY_BUILD_CMDS
	$(HOST_DIR)/bin/TexturePacker -input $(SRC_DIR)/media/ -output $(SRC_DIR)/media/Textures.xbt -dupecheck -use_none
	$(HOST_DIR)/bin/TexturePacker -input $(SRC_DIR)/themes/curial -output $(SRC_DIR)/media/curial.xbt -dupecheck -use_none
	$(HOST_DIR)/bin/TexturePacker -input $(SRC_DIR)/themes/flat -output $(SRC_DIR)/media/flat.xbt -dupecheck -use_none
endef

define KODI_SKIN_ESTUARY_INSTALL_TARGET_CMDS
	mkdir -p $(TARGET_DIR)/usr/share/kodi/addons/skin.estuary
	cp -dpfr $(SRC_DIR)/* $(TARGET_DIR)/usr/share/kodi/addons/skin.estuary
	rm -rf $(TARGET_DIR)/usr/share/kodi/addons/skin.estuary/themes
	find $(TARGET_DIR)/usr/share/kodi/addons/skin.estuary/media -name *.jpg -delete
	find $(TARGET_DIR)/usr/share/kodi/addons/skin.estuary/media -name *.png -delete
endef

$(eval $(generic-package))
