################################################################################
#
# system-settings
#
################################################################################

SYSTEM_SETTINGS_VERSION = $(call qstrip,$(BR2_SYSTEM_SETTINGS_VER))
SYSTEM_SETTINGS_SITE = git@gitlab.com:ember-dev/EmbER-System-Settings-Source.git
SYSTEM_SETTINGS_SITE_METHOD = git
SYSTEM_SETTINGS_INSTALL_STAGING = NO
SYSTEM_SETTINGS_DEPENDENCIES = ifplugd kodi network-manager

CONFIG_FILE = $(@D)/tmp

ifneq ($(BR2_SYSTEM_SETTINGS_GOVERNOR),"")
GOVERNOR = $(subst $\",,$(BR2_SYSTEM_SETTINGS_GOVERNOR))
else
GOVERNOR = performance
endif

ifneq ($(BR2_SYSTEM_SETTINGS_CLOCK),"")
CLOCK = $(subst $\",,$(BR2_SYSTEM_SETTINGS_CLOCK))
else
CLOCK = default
endif

ifeq ($(BR2_SYSTEM_SETTINGS_HASOTA),y)
HAS_OTA = true
OTA_LINK = $(subst $\",,$(BR2_SYSTEM_SETTINGS_UPDATE_URL))
else
HAS_OTA = false
OTA_LINK = false
endif

ifeq ($(BR2_SYSTEM_SETTINGS_UPDATE_WLAN),y)
UPDATE_ID = w
else ifeq ($(BR2_SYSTEM_SETTINGS_UPDATE_ETH),y)
UPDATE_ID = e
else
UPDATE_ID = false
endif

FW_VERSION = $(subst $\",,$(BR2_SYSTEM_SETTINGS_FIRMWARE_VERSION))
HW_CODE = $(subst $\",,$(BR2_SYSTEM_SETTINGS_HARDWARE_CODE))
SETTINGS = $(subst $\",,$(BR2_SYSTEM_SETTINGS_DISABLED))
WIZARD_STEPS = $(subst $\",,$(BR2_SYSTEM_SETTINGS_WIZARD_STEPS))
WIZARD_SET_DEF_SKIN = $(subst $\",,$(BR2_SYSTEM_SETTINGS_WIZARD_SET_DEF_SKIN))

ifeq ($(BR2_SYSTEM_SETTINGS_ISOEM),y)
IS_OEM = true
OEM_DIST = $(subst $\",,$(BR2_SYSTEM_SETTINGS_OEM_DIST))
else
IS_OEM = false
OEM_DIST = Public
endif

CONFIG_TMP = $(@D)/ota_config_src

define SYSTEM_SETTINGS_INSTALL_TARGET_CMDS
	mkdir -p $(TARGET_DIR)/usr/share/sysconfig/
	echo '[Main]' > $(CONFIG_TMP)
	echo 'max_clock = $(CLOCK)' >> $(CONFIG_TMP)
	echo 'default_gov = $(GOVERNOR)' >> $(CONFIG_TMP)
	echo 'disable = $(SETTINGS)' >> $(CONFIG_TMP)
	echo '[Update_Config]' >> $(CONFIG_TMP)
	echo 'has_ota = $(HAS_OTA)' >> $(CONFIG_TMP)
	echo 'firmware_version = $(FW_VERSION)' >> $(CONFIG_TMP)
	echo 'hardware_code = $(HW_CODE)' >> $(CONFIG_TMP)
	echo 'update_address = $(OTA_LINK)' >> $(CONFIG_TMP)
	echo 'read_adapter = $(UPDATE_ID)' >> $(CONFIG_TMP)
	echo '[OEM]' >> $(CONFIG_TMP)
	echo 'is_oem = $(IS_OEM)' >> $(CONFIG_TMP)
	echo 'oem_dist = $(OEM_DIST)' >> $(CONFIG_TMP)
	echo '[WIZARD]' >> $(CONFIG_TMP)
	echo 'wizard_steps = $(WIZARD_STEPS)' >> $(CONFIG_TMP)
	echo 'default_skin = $(WIZARD_SET_DEF_SKIN)' >> $(CONFIG_TMP)
	base64 $(CONFIG_TMP) > $(TARGET_DIR)/usr/share/sysconfig/ota_config
	$(shell cd $(@D) && . BuildAddon.sh)
	sed -i '/       version="/c\       version="$(call qstrip,$(BR2_VERSION))"' $(@D)/output/script.system.settings/addon.xml
	sed -i '/       name="/c\       name="$(call qstrip,$(BR2_SYSTEM_SETTINGS_ADDON_NAME))"' $(@D)/output/script.system.settings/addon.xml
	cp -af $(@D)/output/script.system.settings $(TARGET_DIR)/usr/share/kodi/addons/
	cp -af $(BR2_EXTERNAL_EmbER_PATH)/package/mediacenter/kodi-addons/script-addons/kodi-script-system-settings/system-settings/system/. $(TARGET_DIR)/
	# Addon will handle starting ifplugd
	rm -rf $(TARGET_DIR)/etc/systemd/system/multi-user.target.wants/ifplugd.service
	# Services specific configuration:
	ln -sfn /root/Programs/bin/openvpn $(TARGET_DIR)/usr/sbin/openvpn
endef

$(eval $(generic-package))
