################################################################################
#
# system-settings-overlay
#
################################################################################

SYSTEM_SETTINGS_OVERLAY_VERSION = $(call qstrip,$(BR2_SYSTEM_SETTINGS_OVERLAY_VER))
SYSTEM_SETTINGS_OVERLAY_SITE = $(call qstrip,$(BR2_SYSTEM_SETTINGS_OVERLAY_GIT))
SYSTEM_SETTINGS_OVERLAY_SITE_METHOD = git
SYSTEM_SETTINGS_OVERLAY_INSTALL_STAGING = NO
SYSTEM_SETTINGS_OVERLAY_DEPENDENCIES = system-settings

define SYSTEM_SETTINGS_OVERLAY_INSTALL_TARGET_CMDS
	cp -rf $(@D)/OVERLAY/* $(TARGET_DIR)/usr/share/kodi/addons/script.system.settings/
endef

$(eval $(generic-package))
