################################################################################
#
# kodi-subtitles-subscene
#
################################################################################

KODI_SUBTITLES_SUBSCENE_VERSION = 2ee39cd0a9c291b887a376aee4654163967bd278
KODI_SUBTITLES_SUBSCENE_SITE = $(call github,manacker,service.subtitles.subscene,$(KODI_SUBTITLES_SUBSCENE_VERSION))
KODI_SUBTITLES_SUBSCENE_LICENSE = GPL-2.0
KODI_SUBTITLES_SUBSCENE_LICENSE_FILES = LICENSE.txt
KODI_SUBTITLES_SUBSCENE_DEPENDENCIES = kodi

define KODI_SUBTITLES_SUBSCENE_INSTALL_TARGET_CMDS
	mkdir -p $(TARGET_DIR)/usr/share/kodi/addons/service.subtitles.subscene
	cp -rf $(@D)/* $(TARGET_DIR)/usr/share/kodi/addons/service.subtitles.subscene/
endef

$(eval $(generic-package))
