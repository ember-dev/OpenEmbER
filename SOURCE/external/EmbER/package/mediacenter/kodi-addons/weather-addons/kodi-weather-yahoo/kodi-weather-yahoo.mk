################################################################################
#
# kodi-weather-yahoo
#
################################################################################

KODI_WEATHER_YAHOO_VERSION = 07b9349f8cac5452aeeed8a7cfa507376dd6624a
KODI_WEATHER_YAHOO_SITE = $(call github,ronie,weather.yahoo,$(KODI_WEATHER_YAHOO_VERSION))
KODI_WEATHER_YAHOO_LICENSE = GPL-2.0
KODI_WEATHER_YAHOO_LICENSE_FILES = LICENSE.txt
KODI_WEATHER_YAHOO_DEPENDENCIES = kodi

define KODI_WEATHER_YAHOO_INSTALL_TARGET_CMDS
	mkdir -p $(TARGET_DIR)/usr/share/kodi/addons/weather.yahoo
	cp -rf $(@D)/* $(TARGET_DIR)/usr/share/kodi/addons/weather.yahoo/
endef

$(eval $(generic-package))
