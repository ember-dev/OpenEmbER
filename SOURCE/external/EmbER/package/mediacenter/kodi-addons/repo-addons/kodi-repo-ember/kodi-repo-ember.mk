################################################################################
#
# kodi-repo-ember
#
################################################################################

KODI_REPO_EMBER_VERSION = 0.0.1
KODI_REPO_EMBER_SOURCE = repository.ember.zip
KODI_REPO_EMBER_SITE = https://gitlab.com/ember-dev/repository-ember/raw/master/downloads
KODI_REPO_EMBER_DEPENDENCIES = kodi

define KODI_REPO_EMBER_EXTRACT_CMDS
	unzip -q $(DL_DIR)/$(KODI_REPO_EMBER_SOURCE) -d $(@D)
endef

define KODI_REPO_EMBER_INSTALL_TARGET_CMDS
	mkdir -p $(TARGET_DIR)/usr/share/kodi/addons
	cp -rf $(@D)/repository.ember $(TARGET_DIR)/usr/share/kodi/addons/
endef

$(eval $(generic-package))
