#!/bin/sh
################################################################################
#
# Embedded Entertainment Rom (EmbER)
#
################################################################################

if [ "$1" = "start" ]; then
	if [ ! -f /mnt/data/.kodi-data ]; then
		if [ -f /root/.kodi/temp/.arnubox ] || [ -f /root/.kodi/temp/.data ] || [ -f /root/.data ]; then
			# Remove old lock files
			rm -rf /root/.kodi/temp/.arnubox /root/.kodi/temp/.data /root/.data
		else
			# Extract data.txz
			mkdir -p /root/.kodi
			tar -xJf /usr/share/kodi/kodi-data.txz -C /root/.kodi/
			# Set ownership and permissions
			chown -R root:root /root/.kodi
			find /root/.kodi -type f -exec chmod 0644 {} \;
			find /root/.kodi -type d -exec chmod 0755 {} \;
			find /root/.kodi -type f -name *.sh -exec chmod 0755 {} \;
			find /root/.kodi -type d -name bin -exec chmod -R 0755 {} \;
		fi
		# Create lock file
		echo "CAUTION!!! Removing this file will cause the firmware to reinstall default Kodi data on next boot." > /mnt/data/.kodi-data
		sync
	fi
fi
