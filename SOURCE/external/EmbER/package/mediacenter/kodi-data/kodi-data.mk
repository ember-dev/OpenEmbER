################################################################################
#
# kodi-data
#
################################################################################

ifneq ($(strip $(BR2_KODI_DATA_REV)),"")
KODI_DATA_VERSION = $(call qstrip,$(BR2_KODI_DATA_REV))
else
$(error BR2_KODI_DATA_REV is not set)
endif

ifneq ($(strip $(BR2_KODI_DATA_REPO)),"")
KODI_DATA_SITE = $(call qstrip,$(BR2_KODI_DATA_REPO))
else
$(error BR2_KODI_DATA_REPO is not set)
endif

KODI_DATA_SITE_METHOD = git
KODI_DATA_INSTALL_STAGING = NO

define KODI_DATA_INSTALL_TARGET_CMDS
	$(TAR) -cJvf $(@D)/data.txz -C $(@D)/src addons userdata
	$(INSTALL) -D -m 0644 $(@D)/data.txz $(TARGET_DIR)/usr/share/kodi/kodi-data.txz
endef

define KODI_DATA_INSTALL_INIT_SYSV
	$(INSTALL) -D -m 0755 $(BR2_EXTERNAL_EmbER_PATH)/package/mediacenter/kodi-data/kodi-data.sh $(TARGET_DIR)/etc/init.d/S10setup
endef

define KODI_DATA_INSTALL_INIT_SYSTEMD
	$(INSTALL) -D -m 0755 $(BR2_EXTERNAL_EmbER_PATH)/package/mediacenter/kodi-data/kodi-data.sh $(TARGET_DIR)/usr/bin/ember/kodi-data
	$(INSTALL) -D -m 0644 $(BR2_EXTERNAL_EmbER_PATH)/package/mediacenter/kodi-data/kodi-data.service $(TARGET_DIR)/usr/lib/systemd/system/kodi-data.service
	mkdir -p $(TARGET_DIR)/etc/systemd/system/multi-user.target.wants
	ln -fs ../../../../usr/lib/systemd/system/kodi-data.service $(TARGET_DIR)/etc/systemd/system/multi-user.target.wants/kodi-data.service
endef

$(eval $(generic-package))
