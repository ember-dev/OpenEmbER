################################################################################
#
# speedtest_cli
#
################################################################################

SPEEDTEST_CLI_VERSION = 6603954e450ee08634e44622261d048f84847830
SPEEDTEST_CLI_SITE = $(call github,sivel,speedtest-cli,$(SPEEDTEST_CLI_VERSION))
SPEEDTEST_CLI_INSTALL_STAGING = NO
SPEEDTEST_CLI_LICENSE = Apache-2.0
SPEEDTEST_CLI_LICENSE_FILES = LICENSE

define SPEEDTEST_CLI_INSTALL_TARGET_CMDS
	install -m 755 $(@D)/speedtest.py $(TARGET_DIR)/usr/bin/speedtest-cli
endef

$(eval $(generic-package))
