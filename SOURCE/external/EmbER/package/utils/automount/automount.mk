################################################################################
#
# automount
#
################################################################################

AUTOMOUNT_SITE = $(BR2_EXTERNAL_EmbER_PATH)/package/utils/automount/src
AUTOMOUNT_VERSION = 1.0.1
AUTOMOUNT_SITE_METHOD = local

define AUTOMOUNT_INSTALL_TARGET_CMDS
	install -m 755 -D $(@D)/automount $(TARGET_DIR)/usr/bin/ember/automount
	install -m 644 -D $(@D)/automount@.service $(TARGET_DIR)/usr/lib/systemd/system/automount@.service
	install -m 644 -D $(@D)/99-automount.rules $(TARGET_DIR)/usr/lib/udev/rules.d/99-automount.rules
endef

$(eval $(generic-package))
