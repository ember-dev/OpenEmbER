################################################################################
#
# linaro-aarch64-toolchain
#
################################################################################

ifeq ($(BR2_TOOLCHAIN_GCC_AT_LEAST_7),y)
GCC_VER_MAIN = 7.2
GCC_VER_SUB = 1
GCC_VER_DATE = 2017.11
else ifeq ($(BR2_TOOLCHAIN_GCC_AT_LEAST_6),y)
GCC_VER_MAIN = 6.4
GCC_VER_SUB = 1
GCC_VER_DATE = 2017.11
else ifeq ($(BR2_TOOLCHAIN_GCC_AT_LEAST_5),y)
GCC_VER_MAIN = 5.5
GCC_VER_SUB = 0
GCC_VER_DATE = 2017.10
else ifeq ($(BR2_TOOLCHAIN_GCC_AT_LEAST_4_9),y)
GCC_VER_MAIN = 4.9
GCC_VER_SUB = 4
GCC_VER_DATE = 2017.01
endif

LINARO_AARCH64_TOOLCHAIN_VERSION = $(GCC_VER_MAIN).$(GCC_VER_SUB)-$(GCC_VER_DATE)
LINARO_AARCH64_TOOLCHAIN_SOURCE = gcc-linaro-$(LINARO_AARCH64_TOOLCHAIN_VERSION)-x86_64_aarch64-linux-gnu.tar.xz
LINARO_AARCH64_TOOLCHAIN_SITE = https://releases.linaro.org/components/toolchain/binaries/$(GCC_VER_MAIN)-$(GCC_VER_DATE)/aarch64-linux-gnu

define HOST_LINARO_AARCH64_TOOLCHAIN_INSTALL_CMDS
	mkdir -p $(HOST_DIR)/gcc-linaro-aarch64-linux-gnu
	cp -a $(@D)/* $(HOST_DIR)/gcc-linaro-aarch64-linux-gnu/
endef

$(eval $(host-generic-package))
