################################################################################
#
# ember_vfd
#
################################################################################

EMBER_VFD_VERSION = d33b89cb580e651f92518e2a45222e5b1af69787
EMBER_VFD_SITE = git@gitlab.com:ember-dev/EmbER-VFD-Driver.git
EMBER_VFD_SITE_METHOD = git

ifneq ($(BR2_BOARD_TYPE_AMLOGIC_GX),)
KVER = 3.14.x
else
KVER = 3.10.x
endif

EMBER_VFD_MODULE_SUBDIRS = driver/$(KVER)

define EMBER_VFD_INSTALL_INIT_SYSV
	$(INSTALL) -m 0755 -D $(@D)/script/S04vfd $(TARGET_DIR)/etc/init.d/S04vfd
	$(INSTALL) -m 0755 -D $(@D)/script/vfd-clock $(TARGET_DIR)/etc/init.d/S99vfd-clock
endef

define EMBER_VFD_INSTALL_INIT_SYSTEMD
	$(INSTALL) -m 0644 -D $(@D)/script/vfd.conf $(TARGET_DIR)/usr/lib/modules-load.d/vfd.conf
	$(INSTALL) -m 0755 -D $(@D)/script/vfd-clock $(TARGET_DIR)/usr/bin/ember/vfd-clock
	$(INSTALL) -m 0644 -D $(@D)/script/vfd-clock.service $(TARGET_DIR)/usr/lib/systemd/system/vfd-clock.service
	mkdir -p $(TARGET_DIR)/etc/systemd/system/multi-user.target.wants
	ln -fs ../../../../usr/lib/systemd/system/vfd-clock.service $(TARGET_DIR)/etc/systemd/system/multi-user.target.wants/vfd-clock.service
endef

$(eval $(kernel-module))
$(eval $(generic-package))
