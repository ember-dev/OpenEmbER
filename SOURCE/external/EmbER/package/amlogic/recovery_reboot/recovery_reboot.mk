################################################################################
#
# recovery_reboot
#
################################################################################

RECOVERY_REBOOT_VERSION = 5997b35cdb8af26f45b0c78ff3cd2730012d9225
RECOVERY_REBOOT_SITE = git@gitlab.com:ember-dev/EmbER-Recovery_Reboot.git
RECOVERY_REBOOT_SITE_METHOD = git
RECOVERY_REBOOT_DEPENDENCIES += busybox # install over busybox's 'reboot'

define RECOVERY_REBOOT_BUILD_CMDS
	CC=$(TARGET_CC) IS_AARCH64=$(BR2_aarch64) $(MAKE) -C $(@D)
endef

define RECOVERY_REBOOT_INSTALL_TARGET_CMDS
        install $(@D)/factoryreset $(TARGET_DIR)/usr/sbin
        install $(@D)/otaflash $(TARGET_DIR)/usr/sbin
        install $(@D)/reboot $(TARGET_DIR)/usr/sbin
        install $(@D)/recoveryflash $(TARGET_DIR)/usr/sbin
endef

$(eval $(generic-package))
