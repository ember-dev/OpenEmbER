###############################################################################
#
# libamcodec
#
###############################################################################

LIBAMCODEC_VERSION = 075d810c73aacfaf4c9e81b349bc8a588be15b30
LIBAMCODEC_SITE = git@gitlab.com:ember-dev/EmbER-AmlCodec.git
LIBAMCODEC_SITE_METHOD = git
LIBAMCODEC_DEPENDENCIES = alsa-lib ffmpeg
LIBAMCODEC_LICENSE = Unclear
LIBAMCODEC_INSTALL_STAGING = YES

define LIBAMCODEC_BUILD_CMDS
	$(TARGET_MAKE_ENV) $(MAKE) CC=$(TARGET_CC) CFLAGS="$(TARGET_CFLAGS)" -C $(@D) all
	$(TARGET_MAKE_ENV) $(MAKE) CC=$(TARGET_CC) -C $(@D) install 2> /dev/null
endef

$(eval $(call generic-package))
