################################################################################
#
# mali
#
################################################################################

MALI_VERSION = 1e04d1540fcd486696f2db45a17f494abe86531f
MALI_SITE = git@gitlab.com:ember-dev/EmbER-Amlogic-GPU-Driver.git
MALI_SITE_METHOD = git
MALI_INSTALL_STAGING = YES

ifneq ($(BR2_BOARD_TYPE_AMLOGIC_GX),)
KVER = 3.14.x
else
KVER = 3.10.x
UMP = driver/$(KVER)/ump
UMP_ = ump_
UMP_OPTS = CONFIG_UMP=m
endif

ifeq ($(BR2_arm)$(BR2_BOARD_TYPE_AMLOGIC_GX)$(BR2_BOARD_TYPE_AMLOGIC_M8),yy)
LIB = mali450-armhf.so
else ifeq ($(BR2_BOARD_TYPE_AMLOGIC_GX),y)
LIB = mali450-arm64.so
else
LIB = mali400-armhf.so
endif

MALI_MODULE_SUBDIRS = driver/$(KVER)/mali $(UMP)
MALI_MODULE_MAKE_OPTS = CONFIG_MALI400=m $(UMP_OPTS)

MALI_INSTALL_INIT_SYSV = \
	mkdir -p $(TARGET_DIR)/etc/init.d; \
	install -m 755 $(@D)/script/S04$(UMP_)mali $(TARGET_DIR)/etc/init.d/S04mali

MALI_INSTALL_INIT_SYSTEMD = \
	mkdir -p $(TARGET_DIR)/usr/lib/modules-load.d; \
	install -m 644 $(@D)/script/$(UMP_)mali.conf $(TARGET_DIR)/usr/lib/modules-load.d/mali.conf

MALI_INSTALL_STAGING_CMDS = \
	cp -a $(@D)/library/common/usr/* $(STAGING_DIR)/usr/; \
	install -m 644 $(@D)/library/$(LIB) $(STAGING_DIR)/usr/lib/libMali.so

MALI_INSTALL_TARGET_CMDS = \
	cp -a $(@D)/library/common/* $(TARGET_DIR)/; \
	install -m 644 $(@D)/library/$(LIB) $(TARGET_DIR)/usr/lib/libMali.so

$(eval $(kernel-module))
$(eval $(generic-package))
