################################################################################
#
# amremote
#
################################################################################

AMREMOTE_VERSION = 7536369707fb45731063fb4cd5c6b0886f305a36
AMREMOTE_SITE = git@gitlab.com:ember-dev/EmbER-AmRemote.git
AMREMOTE_SITE_METHOD = git
AMREMOTE_INSTALL_STAGING = NO

AMREMOTE_BUILD_CMDS = $(MAKE) CC=$(TARGET_CC) -C $(@D)

AMREMOTE_INSTALL_TARGET_CMDS = \
	$(INSTALL) -m 755 $(@D)/keytest $(TARGET_DIR)/usr/bin; \
	$(INSTALL) -m 755 $(@D)/remotecfg $(TARGET_DIR)/usr/bin;

ifneq ($(BR2_AMREMOTE_CONFIG),"")
AMREMOTE_INSTALL_TARGET_CMDS += $(INSTALL) -D -m 644 $(BR2_AMREMOTE_CONFIG) $(TARGET_DIR)/etc/amremote/remote.conf
endif

AMREMOTE_INSTALL_INIT_SYSV = $(INSTALL) -D -m 755 $(BR2_EXTERNAL_EmbER_PATH)/package/amlogic/amremote/amremote $(TARGET_DIR)/etc/init.d/S98amremote

AMREMOTE_INSTALL_INIT_SYSTEMD = \
	$(INSTALL) -D -m 755 $(BR2_EXTERNAL_EmbER_PATH)/package/amlogic/amremote/amremote $(TARGET_DIR)/usr/bin/ember/amremote; \
	$(INSTALL) -D -m 644 $(BR2_EXTERNAL_EmbER_PATH)/package/amlogic/amremote/amremote.service $(TARGET_DIR)/usr/lib/systemd/system/amremote.service; \
	mkdir -p $(TARGET_DIR)/etc/systemd/system/multi-user.target.wants; \
	ln -fs ../../../../usr/lib/systemd/system/amremote.service $(TARGET_DIR)/etc/systemd/system/multi-user.target.wants/amremote.service

$(eval $(generic-package))
