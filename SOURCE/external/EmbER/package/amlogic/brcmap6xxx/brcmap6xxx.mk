################################################################################
#
# brcmap6xxx
#
################################################################################

BRCMAP6XXX_VERSION = fbbe3d3fbcfb4e7486dca4832d3e2f5940a29306
BRCMAP6XXX_SITE = git@gitlab.com:ember-dev/EmbER-Amlogic-Broadcom-Driver.git
BRCMAP6XXX_SITE_METHOD = git

BRCMAP6XXX_MODULE_SUBDIRS = driver
BRCMAP6XXX_MODULE_MAKE_OPTS = \
	KCPPFLAGS='-DCONFIG_BCMDHD_FW_PATH=\"/lib/firmware/brcmap6xxx/fw_bcmdhd.bin\" -DCONFIG_BCMDHD_NVRAM_PATH=\"/lib/firmware/brcmap6xxx/nvram.txt\" -DCONFIG_BCMDHD_CONFIG_PATH=\"/lib/firmware/brcmap6xxx/config.txt\"'

ifeq ($(BR2_PACKAGE_BRCMAP6XXX_INIT)$(BR2_PACKAGE_RTLWIFI_INIT),yy)
BRCMAP6XXX_INSTALL_TARGET_CMDS = $(INSTALL) -m 0644 -D $(@D)/misc/79-broadcom_wifi.rules $(TARGET_DIR)/lib/udev/rules.d/79-broadcom_wifi.rules;
else ifeq ($(BR2_PACKAGE_BRCMAP6XXX_INIT),y)
BRCMAP6XXX_INSTALL_INIT_SYSTEMD = $(INSTALL) -m 0644 -D $(@D)/misc/dhd.conf $(TARGET_DIR)/usr/lib/modules-load.d/dhd.conf;
endif

ifeq ($(BR2_PACKAGE_SYSTEMD_HIBERNATE),y)
BRCMAP6XXX_INSTALL_INIT_SYSTEMD += $(INSTALL) -m 0755 -D $(@D)/misc/dhd-suspend $(TARGET_DIR)/usr/lib/systemd/system-sleep/dhd-suspend
endif

ifeq ($(BR2_PACKAGE_NETWORK_MANAGER),y)
BRCMAP6XXX_INSTALL_NM_SCRIPT = $(INSTALL) -m 0544 -D $(@D)/misc/50-dhd-script $(TARGET_DIR)/etc/NetworkManager/dispatcher.d/50-dhd-script;
endif

BRCMAP6XXX_BUILD_CMDS = $(MAKE) CC="$(TARGET_CC)" CFLAGS="$(TARGET_CFLAGS)" -C $(@D)/bin -L $(STAGING_DIR)/usr/lib all

BRCMAP6XXX_INSTALL_TARGET_CMDS += \
	$(BRCMAP6XXX_INSTALL_NM_SCRIPT) \
	mkdir -p $(TARGET_DIR)/lib/firmware/brcmap6xxx; \
	$(INSTALL) -m 0644 $(@D)/firmware/* $(TARGET_DIR)/lib/firmware/brcmap6xxx; \
	$(INSTALL) -m 0644 -D $(@D)/misc/enable_bt@.service $(TARGET_DIR)/usr/lib/systemd/system/enable_bt@.service; \
	$(INSTALL) -m 0644 -D $(@D)/misc/80-broadcom_bt.rules $(TARGET_DIR)/lib/udev/rules.d/80-broadcom_bt.rules; \
	$(INSTALL) -m 0755 -D $(@D)/misc/enable_bt $(TARGET_DIR)/bin/ember/enable_bt; \
	$(INSTALL) -m 0755 -D $(@D)/bin/brcm_patchram_plus $(TARGET_DIR)/bin/brcm_patchram_plus

$(eval $(kernel-module))
$(eval $(generic-package))
