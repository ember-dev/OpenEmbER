################################################################################
#
# Embedded Entertainment Rom (EmbER)
#
################################################################################

# Include Version.conf
-include Version.conf

# Export EmbER Version
export EMBER_VERSION	:= $(EMBER_VERSION)
export EMBER_GIT_REV	:= $(shell git log --format="%h" -n 1)

# Set Variables
TOP_DIR			:= $(shell pwd)
SOURCE_DIR		:= $(TOP_DIR)/SOURCE
FUNCTION_DIR		:= $(TOP_DIR)/FUNCTIONS
DOWNLOAD_DIR		:= $(TOP_DIR)/DOWNLOADS
WORK_DIR		:= $(TOP_DIR)/WORK
PATCH_DIR		?= $(SOURCE_DIR)/patches/EmbER
OVERLAY_DIR		?= $(SOURCE_DIR)/overlay/EmbER
EXTERNAL_DIR		?= $(SOURCE_DIR)/external/EmbER
BR_TAR			:= $(DOWNLOAD_DIR)/buildroot-$(BUILDROOT_VERSION).tar.gz
BR_PATCH_CMD		:= $(FUNCTION_DIR)/apply-patches.sh
BR_TOP_DIR		:= $(WORK_DIR)/buildroot-$(BUILDROOT_VERSION)
BR_OVERLAY_APPLIED	:= $(BR_TOP_DIR)/.overlay_applied
BR_PATCHED		:= $(BR_TOP_DIR)/.patched
BR_TIME_FILE		:= $(BR_TOP_DIR)/.compile_time

ifneq ($(O),)
ifeq ($(shell echo $(O) | head -c1),/)
OUTPUT_DIR := $(O)
else
OUTPUT_DIR := $(TOP_DIR)/$(O)
endif
else
OUTPUT_DIR := $(TOP_DIR)/OUTPUT
endif

ifneq (,$(findstring git@,$(BUILDROOT_URL)))
BR_DL_CMD := git archive --format=tar.gz --prefix=buildroot/ --remote=$(BUILDROOT_URL) $(BUILDROOT_VERSION) >
else
BR_DL_CMD := wget $(BUILDROOT_URL) -O
endif

# Main recipe
main: .ember_create_dirs .ember_prepare_br .ember_compile_br

# Create directories needed for compile
.ember_create_dirs: $(WORK_DIR) $(DOWNLOAD_DIR) $(OUTPUT_DIR)
	@touch $@

$(WORK_DIR) $(DOWNLOAD_DIR) $(OUTPUT_DIR):
	@echo 'MAKING DIR: $@'
	@mkdir -p $@

# Prepare Buildroot
.ember_prepare_br: $(BR_TOP_DIR) $(BR_OVERLAY_APPLIED) $(BR_PATCHED)
	@touch $@

$(BR_TAR):
	@echo 'DOWNLOADING: $@'
	@$(BR_DL_CMD) $@

$(BR_TOP_DIR): $(BR_TAR)
	@echo 'EXTRACTING: $< TO: $(BR_TOP_DIR)/'
	@mkdir -p $(BR_TOP_DIR); tar xfz $< -C $(BR_TOP_DIR) --strip-components 1
	@ln -sf $(DOWNLOAD_DIR) $(BR_TOP_DIR)/dl

$(BR_OVERLAY_APPLIED):
	@if test -d $(OVERLAY_DIR); then \
		echo 'APPLYING OVERLAY FROM: $(OVERLAY_DIR)/ TO: $(BR_TOP_DIR)/'; \
		cp -rf $(OVERLAY_DIR)/. $(BR_TOP_DIR)/; \
	fi
	@touch $@

$(BR_PATCHED):
	@for D in $(PATCH_DIR); do \
		if test -d $${D}; then \
			echo 'APPLY PATCHES FROM: $(PATCH_DIR)/ TO: $(BR_TOP_DIR)/'; \
			$(BR_PATCH_CMD) $(BR_TOP_DIR) $${D} \*.patch \*.diff || exit 1; \
			echo; \
		fi; \
	done
	@touch $@

# Compile Buildroot
.ember_compile_br:
	@date '+%s' > $(BR_TIME_FILE)
	@$(MAKE) O=$(OUTPUT_DIR) BR2_EXTERNAL=$(EXTERNAL_DIR) -C $(BR_TOP_DIR)
	@read st < $(BR_TIME_FILE); st=$$((`date '+%s'` - $$st - 86400)); \
	echo "COMPILE TIME: `date -u -d @$$st '+%H:%M:%S'`"

# Make recipes
clean:
	rm -rf .ember_* $(OUTPUT_DIR) $(WORK_DIR)

contributors:
	@echo "--------------------------------------------------------------------------------"
	@echo
	@cat CONTRIBUTORS.txt
	@echo
	@echo "--------------------------------------------------------------------------------"

distclean: clean
	rm -rf $(DOWNLOAD_DIR)

license:
	@echo "--------------------------------------------------------------------------------"
	@echo
	@cat LICENSE.txt
	@echo
	@echo "--------------------------------------------------------------------------------"

menuconfig: .ember_create_dirs .ember_prepare_br
	@$(MAKE) O=$(OUTPUT_DIR) BR2_EXTERNAL=$(EXTERNAL_DIR) $@ -C $(BR_TOP_DIR)

savedefconfig:
	@$(MAKE) O=$(OUTPUT_DIR) BR2_EXTERNAL=$(EXTERNAL_DIR) $@ -C $(BR_TOP_DIR)

source:
	@$(MAKE) O=$(OUTPUT_DIR) BR2_EXTERNAL=$(EXTERNAL_DIR) $@ -C $(BR_TOP_DIR)

version:
	@echo "--------------------------------------------------------------------------------"
	@echo
	@echo "EmbER $(EMBER_VERSION)~$(EMBER_GIT_REV)"
	@echo
	@echo "--------------------------------------------------------------------------------"

%-build:
	@$(MAKE) O=$(OUTPUT_DIR) BR2_EXTERNAL=$(EXTERNAL_DIR) `basename -s -build $@` -C $(BR_TOP_DIR)

%_defconfig: .ember_create_dirs .ember_prepare_br
	@$(MAKE) O=$(OUTPUT_DIR) BR2_EXTERNAL=$(EXTERNAL_DIR) $@ -C $(BR_TOP_DIR)

%-dirclean:
	@$(MAKE) O=$(OUTPUT_DIR) BR2_EXTERNAL=$(EXTERNAL_DIR) $@ -C $(BR_TOP_DIR)

%-menuconfig:
	@$(MAKE) O=$(OUTPUT_DIR) BR2_EXTERNAL=$(EXTERNAL_DIR) $@ -C $(BR_TOP_DIR)

help:
	@echo
	@echo '################################################'
	@echo '###                                          ###'
	@echo '###    Embedded Entertainment Rom (EmbER)    ###'
	@echo '###                                          ###'
	@echo '################################################'
	@echo
	@echo 'Help:'
	@echo
	@echo '  Cleaning:'
	@echo '    clean                               - delete all files created by build (make clean)'
	@echo '    distclean                           - delete all non-source files (make distclean)'
	@echo
	@echo '  Configuration:'
	@echo '    menuconfig                          - configure build options (make menuconfig)'
	@echo '    *_defconfig                         - select default configuration (make foo_defconfig)'
	@echo '    savedefconfig                       - generate defconfig based configuration (make savedefconfig)'
	@echo
	@echo '  Package options:'
	@echo '    *-build                             - build package (make foo-build)'
	@echo '    *-dirclean                          - delete build directory (make foo-dirclean)'
	@echo '    *-menuconfig                        - configure build options (make foo-menuconfig)'
	@echo
	@echo '  Miscellaneous:'
	@echo '    contributors                        - list contributors (make contributors)'
	@echo '    license                             - display license (make license)'
	@echo '    source                              - download sources for offline build (make source)'
	@echo '    version                             - display version (make version)'
	@echo
	@echo '  Available Configs:'
	@$(foreach b, $(sort $(notdir $(wildcard $(EXTERNAL_DIR)/configs/*_defconfig))), \
		printf "    %-35s - build for %s\\n" $(b) $(b:_defconfig=);)
	@echo

