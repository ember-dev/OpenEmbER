# Embedded Entertainment Rom (EmbER)

**Discription**

EmbER is a Just Enough Operating System (JeOS) built around the award-winning media player Kodi (formerly known as XBMC).

**Donations**

EmbER is a free program that relies on donations to survive, these donations help pay for web hosting, hardware, and other expanses.

**PayPal:**
 cronmod.dev@gmail.com  
**BTC:**
 1EC1qw82VTy49hUwW9RrAJQ4aQNrVXWykZ  
**ETH:**
 0x62bf4783D69e10AAf79fDCE784E401527fc2f362  
**XMR:**
 4AQTBkmpJ22Qr5bdQJbTBe3TRFwxBQ8foNko6QowgZRuLuA8L7bwKKc7uhYvGGMDSqVBm4cDg3Vg8YfGyBHMhKZnFzjwffC

**TODO**

- Implement live OTA
- Add retroarch and emulationstation packages
- Add LibreELEC settings addon
- Add EmbERMod skin for Kodi
